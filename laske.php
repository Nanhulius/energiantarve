<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Energiantarve</title>
</head>
<body>
    <h3>Energiantarpeesi</h3>
    <?php
        $paino = filter_input(INPUT_POST, 'paino', FILTER_SANITIZE_NUMBER_INT);
        $intensiteetti = filter_input(INPUT_POST, 'intensiteetti', FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $sukupuoli = filter_input(INPUT_POST, 'sukupuoli');
        $energia = 0;
        if ($sukupuoli === "mies") {
            $energia = (879 + 10.2 * $paino) * $intensiteetti;
        } else {
            $energia = (795 + 7.18 * $paino) * $intensiteetti;
        }
        printf("<p>Päivittäinen energiantarpeesi on %.1f kaloria", $energia);
    ?>
    <div>
        <a href="index.html">Laske uudestaan</a>
    </div>
</body>
</html>